\input{preamble.tex}


\ifdefined\assemblingchapters \else
\begin{document}
\fi


\section{Extensions of geometric theories}
\label{section-extensions}


\subsection{Background on geometric theories}

For a full definition of geometric theories,
we refer to \cite[Chapter D1.1]{elephant}.
But we want to mention that
a geometric theory $\TT$ can be thought of
as consisting of three \enquote{layers},
first come the \emph{sorts},
then the \emph{relation} and \emph{function symbols},
and finally the \emph{axioms}.
The first two layers are often called the \emph{signature}
of the theory.
What is allowed in each layer
depends on the data in the previous layers:
The set of sorts of $\TT$
is just a set without any additional structure.
The relation and function symbols
have their own signatures,
which in this case just means a list of sorts,
and which we denote
\[ R \subseteq A_1 \times \dots \times A_n
   \qquad\text{and}\qquad
   f : A_1 \times \dots \times A_n \to B
   \rlap{,} \]
where $A_1, \dots, A_n$ and $B$ are sorts of $\TT$.
And the axioms are sequents of the form
\[ \phi \turnstile{x_1 \oftype A_1, \dots, x_n \oftype A_n} \psi
   \rlap{,} \]
to be read as
\enquote{$\phi$ implies $\psi$
in the context $x_1 \oftype A_1, \dots, x_n \oftype A_n$},
where the geometric formulas $\phi$ and $\psi$
can use the relation and function symbols of $\TT$.
The context will sometimes be abbreviated
$\vec{x} \oftype \vec{A}$.

A relation symbol with the empty signature, $n = 0$,
which we might denote $R \subseteq 1$,
is called a \emph{proposition symbol},
and we will rather use the letter $p$ for it.
Similarly,
a function symbol with empty domain, $n = 0$,
is called a \emph{constant symbol},
and instead of $f : 1 \to B$,
we simply denote it as $f : B$,
or rather $c : B$.
A geometric theory is \emph{propositional}
if it has no sorts,
and therefore also no function symbols
and no relation symbols except proposition symbols.

A \emph{model} $M$ of a geometric theory $\TT$
in a topos $\E$
consists of objects
\[ \interpretation{A}_M \in \E \]
for all sorts $A$ of $\TT$,
subobjects
\[ \interpretation{R}_M \subseteq
   \interpretation{A_1}_M \times \dots
   \times \interpretation{A_n}_M \]
for all relation symbols $R$ of $\TT$
and morphisms
\[ \interpretation{f}_M : 
   \interpretation{A_1}_M \times \dots
   \times \interpretation{A_n}_M
   \to \interpretation{B}_M \]
for all function symbols $f$ of $\TT$,
such that the axioms of $\TT$ are fulfilled.
Given a model $M$,
we can not only \emph{interpret}
individual sorts and symbols in $M$,
but also any geometric formula $\phi$
of $\TT$
in a context $x_1 \oftype A_1, \dots, x_n \oftype A_n$,
yielding a subobject
\[ \interpretation{\phi}_M \subseteq
   \interpretation{A_1}_M \times \dots
   \times \interpretation{A_n}_M
   \rlap{.} \]
The requirement that an axiom
\[ \phi \turnstile{\vec{x} \oftype \vec{A}} \psi \]
is fulfilled in $M$
means that there is an inclusion of subobjects
\[ \interpretation{\phi}_M \leq \interpretation{\psi}_M \rlap{.} \]

There is also a notion of \emph{morphism}
between models of the same theory in the same topos,
and the resulting category of $\TT$-models in $\E$
will be denoted
\[ \TT\dashmod(\E) \rlap{.} \]
Furthermore,
the requirement that the axioms of $\TT$ are geometric sequents
ensures that pulling back
the individual parts of a model $M \in \TT\dashmod(\E)$
along a geometric morphism $f : \E' \to \E$
yields a model of $\TT$ in $\E'$.
For a fixed model $M \in \TT\dashmod(\E)$
and varying $f$,
this constitutes a functor
\[ \Geom(\E', \E) \to \TT\dashmod(\E') \rlap{,} \]
and the model $M$ is called a \emph{universal model} of $\TT$
if this functor is an equivalence of categories
for all (Grothendieck) toposes $\E'$.
The topos $\E$ is then called a \emph{classifying topos}
for the geometric theory $\TT$,
and we will call the pair $(\TT, M)$
a \emph{syntactic presentation} of the topos $\E$.

It is a theorem that every (Grothendieck) topos
classifies some geometric theory
and every geometric theory admits a classifying topos,
that is, a universal model in some topos.
The classifying topos of a theory
is also unique up to equivalence,
which justifies writing
\[ \Set[\TT] \]
for a classifying topos of a theory $\TT$.
But it is not at all true that
the classified theory of a topos is unique.
Instead,
two theories admitting universal models in the same topos
are called \emph{Morita equivalent} theories.

There is of course also a notion of provability
for geometric theories,
which we will not define here.
A geometric sequent which is provable in a geometric theory $\TT$
is fulfilled in any model of $\TT$ in any topos,
so it could just as well be added as an axiom of $\TT$.
Two theories $\TT_1$ and $\TT_2$
over the same signature
(same sorts and symbols)
are called \emph{syntactically equivalent}
if every axiom of $\TT_1$ is provable in $\TT_2$
and vice versa.
Note
that this is a much stronger condition on $\TT_1$ and $\TT_2$
than being Morita equivalent,
since Morita equivalent theories
can have different signatures.
We will simply write syntactic equivalence as equality,
\[ \TT_1 = \TT_2 \rlap{.} \]

The universal model of a theory $\TT$
is unique in the sense that
for any two universal models in toposes $\E_1$ and $\E_2$,
there is an equivalence $\E_1 \simeq \E_2$
sending one to the other.
With respect to provability,
it has the following strong property.
A geometric sequent of a theory $\TT$
is fulfilled in the universal model of $\TT$
if and only if
it is provable in $\TT$.

Finally,
we would like to make the point that
when manipulating geometric theories,
one has to think intuitionistically.
This is not the case when the matter is only about axioms;
it is a well-known theorem
that any geometric sequent
which is provable from the axioms of a geometric theory
using full classical first-order logic
is also provable from these axioms in geometric logic.
But if we are also interested in adding sorts and symbols,
the intuitionistic nature shows clearly.
For example,
the theory consisting of a single proposition symbol $p$
has two models (up to isomorphism) in $\Set$,
as the interpretation of $p$ can be either true or false.
The same is true for the theory with
two proposition symbols $p$ and $q$
and the axioms
\[ \top \turnstile{[]} p \lor q
   \quad\text{and}\quad
   p \land q \turnstile{[]} \bot
   \rlap{.}
   \]
But these are two completely different theories,
as a model of the first theory in a topos $\E$
is just an open subtopos of $\E$,
while a model of the second theory
is a decomposition of $\E$ into two subtoposes
which are both open and closed.
And even the categories of models in $\Set$
are not equivalent,
since proposition symbols are allowed to become true
but not to become false
under model homomorphisms.


\subsection{Theory extensions as presentations of geometric morphisms}

\begin{definition}
  A (geometric) \emph{extension} $\EE$ of a geometric theory $\TT$
  % could-do: find better symbol for extensions?
  consists of a set $\sortsset{\EE}$ of sorts,
  sets $\relsset{\EE}$ and $\funsset{\EE}$
  of relation and function symbols
  over the sorts $\sortsset{\TT} \sqcup \sortsset{\EE}$
  and a set of geometric axioms
  over the sorts $\sortsset{\TT} \sqcup \sortsset{\EE}$
  and the symbols $\relsset{\TT} \sqcup \relsset{\EE}$
  and $\funsset{\TT} \sqcup \funsset{\EE}$.
  We denote $\TT + \EE$ the theory obtained by
  adding these sorts, symbols and axioms to~$\TT$.
  The extension $\EE$ is \emph{localic}
  if $\sortsset{\EE} = \varnothing$;
  it is a \emph{quotient extension}
  if additionally $\relsset{\EE} = \varnothing$
  and $\funsset{\EE} = \varnothing$.
\end{definition}

If $\EE$ is an extension of $\TT$,
we have a forgetful functor
\[ U_\EE : \mod{(\TT + \EE)}{\E} \to \mod{\TT}{\E} \]
for every Grothendieck topos $\E$.
Note that this functor is an isofibration.
Also,
after fixing classifying toposes $\Set[\TT]$ and $\Set[\TT + \EE]$,
the $\TT$-model part of the universal $(\TT + \EE)$-model
induces a canonical geometric morphism
\[ \pi_\EE : \Set[\TT + \EE] \to \Set[\TT] \rlap{,} \]
which in turn acts on generalized points
by the functors $U_\EE$
(up to natural isomorphism).
This is the geometric morphism
\emph{presented} by the extension $\EE$.

The following theorem
says that every geometric morphism
can be presented in this way,
thus generalizing the result that
every Grothendieck topos classifies a geometric theory
to the relative situation
over some base topos $\Set[\TT]$
with an already chosen syntactic presentation.

\begin{theorem}[{\cite[Theorem 7.1.5]{caramello:tst}}]
  \label{theorem-extension-from-geometric-morphism}
  Let a geometric morphism
  \[ p : \E \to \Set[\TT] \]
  to the classifying topos of a geometric theory $\TT$
  be given.
  Then $p$ is, up to isomorphism,
  of the form $\pi_\EE : \Set[\TT + \EE] \to \Set[\TT]$
  for some extension $\EE$ of $\TT$.
  If $p$ is localic (respectively an embedding),
  then we can take $\EE$ to be a localic extension
  (respectively a quotient extension).
\end{theorem}

\begin{proof}
  See \cite[Theorem 7.1.5]{caramello:tst}.
  The case of an embedding,
  which is not mentioned there,
  is instead part of the duality
  between subtoposes and quotient theories
  \cite[Theorem 3.2.5]{caramello:tst}.
\end{proof}

Given two extensions $\EE_1$ and $\EE_2$ of a theory $\TT$,
it is clear that we can also regard $\EE_2$
as an extension of $\TT + \EE_1$ and vice versa.
Then we have
a strict pullback diagram of categories
\[ \begin{tikzcd}
  (\TT + \EE_1 + \EE_2)\dashmod(\E) \ar[r] \ar[d]
  \ar[dr, phantom, very near start, "\lrcorner"] &
  (\TT + \EE_2)\dashmod(\E) \ar[d] \\
  (\TT + \EE_1)\dashmod(\E) \ar[r] &
  \TT\dashmod(\E)
\end{tikzcd} \]
for any topos $\E$,
which is also a weak pullback,
since the the forgetful functors are isofibrations.
This means that $\Set[\TT + \EE_1 + \EE_2]$
is the pullback topos
of $\Set[\TT + \EE_1]$ and $\Set[\TT + \EE_2]$ over $\Set[\TT]$.
In particular,
the product
(as generalized spaces, not as categories)
of two classifying toposes $\Set[\TT_1]$ and $\Set[\TT_2]$
classifies the theory $\TT_1 + \TT_2$.

In the same way in which
we prefer to have a symbol for an extension $\EE$
instead of only for the extended theory $\TT' = \TT + \EE$,
we will also want to regard the data
that is missing in a $\TT$-model
compared to a $(\TT + \EE)$-model
as an object in its own right.

\begin{definition}
  A \emph{model extension} $E$ of a model $M \in \mod{\TT}{\E}$
  along a theory extension $\EE$ of $\TT$
  consists of interpretations for the sorts and symbols of $\EE$
  that extend $M$ to a model $M + E \in \mod{(\TT + \EE)}{\E}$.
  A \emph{homomorphism} of model extensions of $M$
  is a family of maps,
  one for each sort of $\EE$,
  that constitutes a $(\TT + \EE)$-model homomorphism
  when combined with the identity homomorphism of $M$.
  That is,
  the category of model extensions of $M$ along $\EE$
  is isomorphic to the strict preimage of $M$ under $U_\EE$;
  it will be denoted $\mod{\EE}{\E, M}$
  or simply $\mod{\EE}{M}$.
\end{definition}

\begin{remark}
  Note that our terminology here
  is somewhat in conflict with
  the usage of for example \enquote{elementary extension}
  in set theory,
  which means a bigger model of the same theory.
\end{remark}

Using the notion of model extensions,
Theorem \ref{theorem-extension-from-geometric-morphism}
can be formulated as follows.
Given any model $M \in \TT\dashmod(\E)$
of a geometric theory in some topos,
there is always an extension $\EE$ of $\TT$
and a model extension $E$ of $M$ along $\EE$
such that the model $(M + E) \in (\TT + \EE)\dashmod(\E)$
is universal.


\subsection{Equivalence extensions}

\begin{definition}
  An \emph{equivalence extension}
  is an extension $\EE$ of $\TT$
  such that the forgetful functor
  $U_\EE : \mod{(\TT + \EE)}{\E} \to \mod{\TT}{\E}$
  is an equivalence of categories
  for every Grothendieck topos $\E$.
  Equivalently,
  the geometric morphism $\pi_\EE : \Set[\TT + \EE] \to \Set[\TT]$
  presented by $\EE$ is an equivalence.
\end{definition}

If $\EE$ is an equivalence extension of a theory $\TT$,
then it is also an equivalence extension of $\TT + \EE'$,
for any other extension $\EE'$ of $\TT$.
This is clear from the (weak) pullback property
of the category $(\TT + \EE_1 + \EE_2)\dashmod(\E)$.

\begin{lemma}
  \label{lemma-equivalence-extension-if-unique-model-extensions}
  An extension $\EE$ of a theory $\TT$
  is an equivalence extension
  if and only if
  every model $M \in \mod{\TT}{\E}$
  in every Grothendieck topos $\E$
  admits exactly one model extension along $\EE$
  up to isomorphism.
\end{lemma}

\begin{proof}
  The given condition means
  that $U_\EE : \mod{(\TT + \EE)}{\E} \to \mod{\TT}{\E}$
  is bijective on isomorphism classes
  for every topos $\E$,
  which at first sight seems weaker than being an equivalence.
  But it means that the functors
  $\Geom(\E, \Set[\TT + \EE]) \to \Geom(\E, \Set[\TT])$
  induced by the geometric morphism
  $\pi_\EE : \Set[\TT + \EE] \to \Set[\TT]$
  are bijective on isomorphism classes,
  in other words,
  $\pi_\EE$ is an isomorphism in the 1-category
  of toposes and geometric morphisms up to isomorphism,
  which is the same as an equivalence of toposes.
\end{proof}

\begin{remark}
  Lemma \ref{lemma-equivalence-extension-if-unique-model-extensions}
  is a version of the slogan that
  if we know the models of a geometric theory,
  we also know the morphisms between them,
  see \cite[below Lemma B4.2.3]{elephant}.
  However, here it does not suffice to simply say that
  a morphism of $\TT$-models in $\E$
  is the same as a $\TT$-model in the topos $\E^{\to}$
  (the arrow category of $\E$),
  as $U_\EE$ can be bijective on isomorphism classes
  for both $\E$ and $\E^{\to}$
  without being an equivalence for $\E$.
  For example,
  let $\TT = \varnothing$,
  let $\EE$ be the theory of $G$-torsors for a group $G$
  and let $\E = \Set$.
  Denote $\underline{G}$
  the one object groupoid associated to $G$.
  Then both of the functors
  \begin{gather*}
    \underline{G} \simeq \mod{(\TT + \EE)}{\Set} \to
    \mod{\TT}{\Set} \simeq \{*\} \rlap{,}
    \\
    \underline{G} \simeq \underline{G}^{\to}
    \simeq (\TT + \EE)\dashmod(\Set^{\to})
    \to
    \TT\dashmod(\Set^{\to}) \simeq
    \{*\}^{\to} \simeq \{*\} \rlap{,}
  \end{gather*}
  are bijective on isomorphism classes,
  but the first is not an equivalence.
\end{remark}

\begin{lemma}
  \label{lemma-equivalence-extension-of-universal-model}
  Let $M \in \mod{\TT}{\E}$ be a universal model,
  $\EE$ an extension of $\TT$
  and $E$ a model extension of $M$ along $\EE$.
  Then $\EE$ is an equivalence extension
  if and only if
  $M + E \in \mod{(\TT + \EE)}{\E}$ is again universal.
\end{lemma}

\begin{proof}
  This is immediate from the two-out-of-three property
  of equivalences of categories
  in the diagram
  \[ \begin{tikzcd}[row sep=small, column sep=large,
      baseline=(current bounding box.south)]
    & \mod{(\TT + \EE)}{\E'} \ar[dd, "U_\EE"] \\
    \mathrm{Geom}(\E', \E)
    \ar[ru, "\hole^*(M + E)"] \ar[rd, "\hole^*M"'] & \\
    & \mod{\TT}{\E'}
    \rlap{.}
  \end{tikzcd} \qedhere \]
\end{proof}

Apart from syntactic equivalences,
one way to produce an equivalence extension of a theory $\TT$
is to add new symbols
for relations or functions that were already definable in $\TT$.
We now want to show that
all localic equivalence extensions are of this form
up to syntactic equivalence,
meaning that we have a simple syntactic characterization
of equivalence extensions among the localic extensions.

\begin{definition}
  \label{definition-extension-by-definitions}
  Let $\TT$ be a geometric theory,
  let $R_i \subseteq \vec{A}^i$
  and $f_j : \vec{B}^j \to C^j$
  be families of new relation and function symbols
  (with signatures consisting of sorts of $\TT$)
  and let $\{ \vec{x} \oftype \vec{A}^i \,.\; \phi_i(\vec{x}) \}$
  and $\{ \vec{y} \oftype \vec{B}^j, z \oftype C^j \,.\;
  \psi_j(\vec{y}, z) \}$
  be formulas of $\TT$ in corresponding contexts,
  where every $\psi_j$ is provably functional.
  Then the \emph{extension by definitions} for this data
  is the localic extension of $\TT$
  consisting of the symbols $R_i$, $f_j$
  and the axioms
  \[ R_i(\vec{x}) \doubleturnstile{\vec{x} \oftype \vec{A}^i}
  \phi_i(\vec{x})
  \qquad \text{and} \qquad
  f_j(\vec{y}) = z \doubleturnstile{\vec{y} \oftype \vec{B}^j, z \oftype C^j}
  \psi_j(\vec{y}, z) . \]
\end{definition}

\begin{remark}
  \label{remark-recognizing-extensions-by-definitions}
  To show that a given localic extension $\EE$
  of a theory $\TT$ is (syntactically equivalent to)
  an extension by definitions,
  one has to find formulas $\phi_i$, $\psi_j$ of $\TT$,
  the latter provably functional,
  for the relation and function symbols of $\EE$,
  such that the axioms in
  Definition \ref{definition-extension-by-definitions}
  are provable in $\TT + \EE$,
  and furthermore,
  one has to check that all axioms of $\EE$
  are already provable in $\TT$
  if the symbols $R_i$, $f_j$
  are replaced by the formulas $\phi_i$, $\psi_j$.
  However,
  this last check can be omitted
  if a model extension $E$ along $\EE$
  of a universal $\TT$-model $M$ is available,
  since then any $\TT$-sequent which is true in $M + E$
  is trivially also true in $M$
  and therefore provable in $\TT$.
  For the same reason,
  it is then automatic that
  the $\psi_j$ are provably functional.
  This will be relevant in applications of
  Corollary \ref{corollary-localic-gluing}.
\end{remark}

\begin{proposition}
  \label{proposition-extensions-by-definitions}
  A localic extension
  is an equivalence extension
  if and only if it is
  (syntactically equivalent to)
  an extension by definitions.
\end{proposition}

\begin{proof}
  If $\EE$ is an extension by definitions of $\TT$
  then any model $M \in \mod{\TT}{\E}$
  admits exactly one model extension $E$ along $\EE$,
  namely $\interpretation{R_i}_{M + E} \defeq
  \interpretation{\phi_i}_M$
  and $\interpretation{f_i}_{M + E}$ is the morphism with graph
  $\interpretation{\psi_j}_M$.
  So $\EE$ is an equivalence extension by Lemma
  \ref{lemma-equivalence-extension-if-unique-model-extensions}.

  On the other hand,
  if $\EE$ is an equivalence extension of $\TT$,
  then consider a universal model $M \in \mod{\TT}{\Set[\TT]}$
  and the unique (up to isomorphism) extension $E$ of $M$ along $\EE$.
  By Lemma \ref{lemma-equivalence-extension-of-universal-model},
  $M + E$ is a universal model of $\TT + \EE$.
  Now, for every relation symbol $R \subseteq \vec{A}$
  introduced in $\EE$,
  $\interpretation{R}_{M + E}$ is a subobject of
  $\interpretation{A_1}_M \times \dots \times \interpretation{A_n}_M$
  and since $M$ is universal,
  \cite[Theorem 6.1.3]{caramello:tst} tells us that
  there is a formula $\phi_R$ of $\TT$
  with $\interpretation{\phi_R}_M = \interpretation{R}_{M + E}$.
  Similarly, for every new function symbol $f$
  we find a provably functional formula $\psi_f$ of $\TT$
  such that $\interpretation{\psi_f}_M$
  is the graph of $\interpretation{f}_{M + E}$.
  That is, the axioms for defining $R$ by $\phi_R$ and $f$ by $\psi_f$
  are fulfilled for $M + E$
  and therefore provable in $\TT + \EE$.
  Thus,
  we have $\EE = \EE_1 + \EE_2$
  where $\EE_1$ is an extension by definitions
  and $\EE_2$ is a quotient.
  But since both $\EE_1$ and $\EE_1 + \EE_2$ are equivalence extensions,
  $\EE_2$ must be one too,
  and we have (up to syntactic equivalence) $\EE_2 = \varnothing$
  and $\EE = \EE_1$.
\end{proof}

The following lemma is about \enquote{reverting}
an extension $\EE$ of $\TT$ by applying another extension $\EE'$,
subject to extensibility of the universal $\TT$-model along $\EE$.

\begin{lemma}
  \label{lemma-revert-an-extension}
  Let $M \in \mod{\TT}{\E}$ be a universal model
  and $E$ an extension of $M$ along some theory extension $\EE$.
  Then there is a localic extension $\EE'$ of $\TT + \EE$
  and a model extension $E'$ of $M + E$ along $\EE'$
  such that $(M + E + E') \in \mod{(\TT + \EE + \EE')}{\E}$
  is again universal.
  (In particular, $\EE + \EE'$ is an equivalence extension.)
  If $\EE$ is localic,
  $\EE'$ can be chosen as a quotient;
  if $\EE$ is a quotient,
  $\EE' = \varnothing$ fits the bill.
  \[ \begin{tikzcd}[row sep=small]
    \TT + \EE + \EE' \\
    \TT + \EE \ar[u, no head] \\
    \TT \ar[u, no head]
    \ar[uu, bend left=60, no head, "\sim"] % "\rotatebox{110}{$\sim$}"]
  \end{tikzcd} \]
\end{lemma}

\begin{proof}
  Fix a classifying topos $\E_{\TT + \EE}$ for $\TT + \EE$.
  We have the forgetful geometric morphism $u : \E_{\TT + \EE} \to \E$,
  and $M + E \in \mod{(\TT + \EE)}{\E}$ corresponds to
  a section (up to isomorphism) $s : \E \to \E_{\TT + \EE}$ of $u$.
  As such, $s$ is localic (since $u \circ s$ is).
  By Theorem \ref{theorem-extension-from-geometric-morphism},
  it is therefore presented by some localic extension $\EE'$
  of $\TT + \EE$,
  that is,
  we have a universal model of $\TT + \EE + \EE'$ in $\E$
  extending $M + E$, as required.

  The two special cases follow since
  a section of a localic geometric morphism is an embedding
  and a section of an embedding is an equivalence,
  but we can also show them more directly.
  If $\EE = \QQ$ is a quotient,
  then the existence of $E$ just means that
  the new axioms are fulfilled in $M$.
  But this means that they were already provable in $\TT$
  and $M = M + E$ is indeed a universal model of $\TT + \QQ$.
  For $\EE$ localic,
  we have to deal with the new relation symbols $R$
  and function symbols $f$.
  From \cite[Theorem 6.1.3]{caramello:tst}
  we know that
  $\interpretation{R(\vec{x})}_{M + E} =
  \interpretation{\phi_R(\vec{x})}_M$
  and
  $\interpretation{f(\vec{x}) = y}_{M + E} =
  \interpretation{\phi_f(\vec{x}, y)}_M$
  for some formulas $\phi_R$, $\phi_f$,
  where $\phi_f$ is provably functional.
  So taking for $\EE'$ the axioms
  $R(\vec{x}) \doubleturnstile{\vec{x}} \phi_R(\vec{x})$
  and
  $f(\vec{x}) = y \doubleturnstile{\vec{x}, y} \phi_f(\vec{x}, y)$,
  we obtain as $\EE + \EE'$ an extension by definitions.
\end{proof}

As a corollary,
we find that an equivalence between two theories,
and more generally an equivalence between
two extensions of some base theory
(meaning that they present the same geometric morphism),
can always be captured in a syntactic way.

\begin{corollary}
  \label{corollary-diagonal-extension}
  Let $\EE_1$, $\EE_2$ be two equivalent extensions of a theory $\TT$,
  that is,
  there exists a model $M \in \mod{\TT}{\E}$
  with extensions to universal models
  $M + E_1$ and $M + E_2$ of $\TT + \EE_1$ respectively $\TT + \EE_2$
  in the same topos $\E$.
  Then there is a localic extension $\EE_{1, 2}$
  of $\TT + \EE_1 + \EE_2$
  such that both $\EE_2 + \EE_{1, 2}$ and $\EE_1 + \EE_{1, 2}$
  are equivalence extensions
  (of $\TT + \EE_1$ respectively $\TT + \EE_2$)
  \[ \begin{tikzcd}[column sep=tiny]
    & \TT + \EE_1 + \EE_2 + \EE_{1, 2} & \\
    \TT + \EE_1 \ar[ur, no head, "\sim"] & &
    \TT + \EE_2 \ar[ul, no head, "\sim"'] \\
    & \TT \ar[ul, no head] \ar[ur, no head] &
  \end{tikzcd} \]
  and there is a model extension $E_{1, 2}$ along $\EE_{1, 2}$
  such that $M_0 + E_1 + E_2 + E_{1, 2}$ is universal.
  If $\EE_1$ or $\EE_2$ is localic,
  then $\EE_{1, 2} = \QQ_{1, 2}$ can be chosen as
  a quotient extension.
\end{corollary}

\begin{proof}
  Regard $\EE_2$ as an extension of $\TT + \EE_1$ for the moment,
  then we have an extension $E_2$ of the universal model $M + E_1$,
  so by Lemma \ref{lemma-revert-an-extension}
  there exists a localic extension $\EE_{1, 2}$
  of $\TT + \EE_1 + \EE_2$
  together with a model extension $E_{1, 2}$
  of $M + E_1 + E_2$
  such that $M + E_1 + E_2 + E_{1, 2}$ is universal.
  Then, by Lemma \ref{lemma-equivalence-extension-of-universal-model},
  both $\EE_2 + \EE_{1, 2}$ and $\EE_1 + \EE_{1, 2}$
  are equivalence extensions, as required.
  If $\EE_1$ is localic,
  we get a quotient extension $\EE_{1, 2} = \QQ_{1, 2}$ from
  Lemma \ref{lemma-revert-an-extension},
  and if $\EE_2$ is localic, we swap the two.
\end{proof}

A completely different proof of
Corollary \ref{corollary-diagonal-extension}
in the absolute case $\TT = \varnothing$
can be found in \cite[Theorem 5.1]{syntactic-morita}.

\begin{definition}
  \label{definition-diagonal-extension}
  We call an extension $\EE_{1, 2}$ as in
  Corollary \ref{corollary-diagonal-extension}
  a \emph{diagonal extension} for $\EE_1$ and $\EE_2$
  over $\TT$,
  because it presents the diagonal geometric morphism
  of the pullback topos
  \[ \Set[\TT + \EE_1 + \EE_2] =
     \Set[\TT + \EE_1] \times_{\Set[\TT]} \Set[\TT + \EE_2] =
     \E \times_{\Set[\TT]} \E
     \rlap{.} \]
\end{definition}

Diagonal extensions are not unique.
For example,
if $\TT_1 = \angles{p_1} + \angles{p_2}$
and $\TT_2 = \angles{p_3} + \angles{p_4}$
both consist of two proposition symbols,
there are clearly two different diagonal quotient extensions
for $\TT_1$ and $\TT_2$ over $\varnothing$.
Corollary \ref{corollary-diagonal-extension}
produces a diagonal extension
in accordance with the two universal models
$M_0 + E_1$ and $M_0 + E_2$ living in the same topos.

Another easy consequence of
Lemma \ref{lemma-revert-an-extension}
is that any object of the classifying topos of a theory
can be introduced into the theory as a new sort
by means of an equivalence extension.

\begin{corollary}
  Let $M \in \mod{\TT}{\E}$ be a universal model
  and let $X \in \E$ be any object.
  Then there is an equivalence extension $\EE$ of $\TT$
  containing exactly one new sort $A$,
  such that the unique (up to unique isomorphism)
  model extension $E$ of $M$ along $\EE$
  interprets $A$ as $X$.
\end{corollary}

\begin{proof}
  Apply Lemma \ref{lemma-revert-an-extension}
  to the extension $\EE_1$ adding nothing but the sort $A$
  (and the model extension $E_1$
  given by $\interpretation{A}_{E_1} \defeq X$)
  to obtain a localic extension $\EE_2$,
  and set $\EE \defeq \EE_1 + \EE_2$.
\end{proof}

% could-do: relate to "geometric constructions" (Vickers)


\subsection{Some examples of equivalence extensions}

Giving examples of localic equivalence extensions
does not seem necessary after
Proposition \ref{proposition-extensions-by-definitions},
but here are some non-localic equivalence extensions
that will be of use later.

\begin{example}
  \label{example-materialize-subobject}
  Let $A$ be a sort of a geometric theory $\TT$
  and let $\phi(x)$ be a geometric formula
  in the context $x \oftype A$.
  Then there is an equivalence extension of $\TT$
  consisting of
  a sort $S_\phi$,
  a function symbol $\iota : S_\phi \to A$
  and the axioms
  \[ \iota(x) = \iota(y) \turnstile{x, y \oftype S_\phi} x = y
     \qquad\text{and}\qquad
     \ex{x}{S_\phi} \iota(x) = y \doubleturnstile{y \oftype A} \phi(y)
     \rlap{,} \]
  the first of which forces the interpretation of $\iota$ in a model
  to be a monomorphism,
  while the second ensures that
  $\interpretation{S_\phi} \hookrightarrow \interpretation{A}$
  is the same subobject as
  $\interpretation{\phi} \hookrightarrow \interpretation{A}$.
\end{example}

\begin{example}
  \label{example-materialize-quotient}
  Let $A$ be a sort of a geometric theory $\TT$
  and let $x \sim x'$ be a geometric formula
  in the context $x, x' \oftype A$
  such that the usual axioms of an equivalence relation
  (which are Horn sequents)
  are provable for $\sim$.
  Then there is an equivalence extension of $\TT$
  consisting of a sort $A/{\sim}$,
  a function symbol $\pi : A \to A/{\sim}$
  and the axioms
  \[ \top \turnstile{y \oftype A/{\sim}} \ex{x}{A} \pi(x) = y
     \qquad\text{and}\qquad
     \pi(x) = \pi(x') \doubleturnstile{x, x' \oftype A} x \sim x'
     \rlap{.} \]
  These force the interpretation
  $\interpretation{A} \to \interpretation{A/{\sim}}$
  of $\pi$
  to be an epimorphism
  with kernel pair
  $\interpretation{\sim} \rightrightarrows \interpretation{A}$.
\end{example}

\begin{remark}
  An intermediate notion between
  syntactic equivalence and Morita equivalence
  is the notion of (geometric) \emph{bi-interpretability},
  see \cite[Definition 2.1.13]{caramello:tst}.
  Two geometric theories are bi-interpretable
  if and only if their syntactic sites are equivalent categories.
  So while this notion is still syntactic in nature,
  it does not assume any previously given relation
  between the signatures of the two theories.
  For example, a sort $A$ of the first theory,
  which is represented in the syntactic site
  by the object $\{x \oftype A.\; \top\}$,
  can correspond to any formula in context
  $\{ \vec{y} \oftype \vec{B}.\; \phi(\vec{y}) \}$
  of the second theory.

  The equivalence extension in
  Example \ref{example-materialize-subobject}
  induces a bi-interpretation,
  interpreting the new sort $S_\phi$
  by the formula in context
  $\{x \oftype A.\; \phi(x) \}$
  of $\TT$.
  But already Example \ref{example-materialize-quotient}
  shows that bi-interpretability is a stronger condition
  than Morita equivalence,
  since the formula in context $\{x \oftype A/{\sim}.\; \top\}$
  can not be expressed as any formula in context of $\TT$.
\end{remark}

\begin{example}
  \label{example-import-a-set}
  Given a set $A$,
  define a theory $\underline{A}$
  (named after its only sort)
  consisting of
  a sort $\underline{A}$,
  constant symbols $c_a : \underline{A}$
  for every element $a \in A$
  and the axioms
  \[ c_a = c_{a'} \turnstile{[]} \bot
     \quad\text{for $a \neq a' \in A$},
     \qquad \qquad
     \top \turnstile{x : \underline{A}} \bigvee_{a \in A} (x = c_a)
     \rlap{.} \]
  One can check that the unique model (up to unique isomorphism)
  in any Grothendieck topos is the constant sheaf
  associated to the set $A$,
  which is also denoted $\underline{A}$.
  The theory $\underline{A}$
  is therefore Morita-equivalent to the empty theory,
  and adding $\underline{A}$ to any given theory
  is an equivalence extension.
  In other words,
  we can always \emph{import} a set $A$
  into our theory
  without changing it up to Morita-equivalence.

  If we have a function like $f : A \to B$
  or a relation like $R \subseteq A$,
  we can import it together with the respective sets.
  Namely,
  after adding a function symbol
  $\underline{f} : \underline{A} \to \underline{B}$
  or a relation symbol
  $\underline{R} \subseteq \underline{A}$,
  the axioms
  \[ \top \turnstile{[]} \underline{f}(c_a) = c_{f(a)}
     \quad\text{for $a \in A$} \]
  respectively
  \[ \top \turnstile{[]} \underline{R}(c_a)
     \quad\text{for $a \in R$},
     \qquad \qquad
     \underline{R}(c_a) \turnstile{[]} \bot
     \quad\text{for $a \in A \setminus R$}
     \]
  produce an extension by definitions
  in presence of the axioms of $\underline{A}$.

  Another perspective on this is
  that if we have any model of a geometric theory $\TT$ in $\Set$,
  then by Lemma \ref{lemma-revert-an-extension}
  there is a localic extension to a universal model in $\Set$,
  which then yields,
  by pulling back along the unique geometric morphism to $\Set$,
  the unique model of the extended theory in any topos.
\end{example}


\ifdefined\assemblingchapters \else
\bibliography{bibliography.bib}
\end{document}
\fi
