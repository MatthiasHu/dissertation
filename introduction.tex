\input{preamble.tex}


\ifdefined\assemblingchapters \else
\begin{document}
\fi


\section{Introduction}

This thesis elaborates in various ways
on the theme in topos theory
that a Grothendieck topos can be viewed as
\emph{the essence of a geometric theory}.
Formally,
one says that a Grothendieck topos $\E$
\emph{classifies} the geometric theory $\TT$
if the models of $\TT$ in any Grothendieck topos $\E'$
correspond to the geometric morphisms from $\E'$ to $\E$.
Since the topos $\E$ is then uniquely determined
by the theory $\TT$,
we can take the dual standpoint
that the theory $\TT$ is a \emph{presentation}
for the topos $\E$.
(All theories that we will meet
will be geometric theories,
and the term topos will always mean Grothendieck topos.)

One reason why we might seek such a presentation for a given topos
is that it can be much more concise
and, we would argue, even more intuitive
than a definition of the same topos by a site.
The prime example for this,
originating in \cite{hakim},
is the big Zariski topos $\E = (\Spec \ZZ)_\Zar$
of the affine scheme $\Spec \ZZ$.
A site of definition for $\E$
is given by the opposite category of
the category of all finitely presentable rings,
equipped with a certain Grothendieck topology
called the Zariski topology,
which involves localizations $A_a$
of a ring $A$ at an element $a \in A$
and the condition that some elements
generate the unit ideal $(1) = A$.
On the other hand,
one can also define $\E$ as
the classifying topos of the theory of local rings.
Here,
a full definition consists simply in
writing down the usual algebraic operations and axioms
defining a (commutative, unitary) ring
and the extra axiom that the ring be local,
stated in the elementary form that if a sum of elements is invertible,
then one of these elements is invertible too.
Of course, to get an actual Grothendieck topos out of this,
one needs the whole machinery of classifying toposes,
but the presentation itself is quite short and very approachable.
To a certain degree,
it is even possible to judge
manipulations of the syntactic presentation correctly,
based on nothing but intuition from elementary algebra.
For example,
the classifying topos stays the same if we add to the list of axioms
a redundant one like $(xy)z = (zy)x$,
but not if we add an axiom like $x = -x$.

Another reason is simply that
a classified geometric theory for a topos $\E$
is a description of the representable functor $\Hom(\hole, \E)$,
that is,
it is a definition for $\E$ by a universal property.
We would like to stress that
since Grothendieck toposes form a 2-category,
the representable functor $\Hom(\hole, \E)$
is in fact a pseudofunctor,
from the 2-category of Grothendieck toposes
to the 2-category of categories.
Such a pseudofunctor comprises
a huge amount of data,
and it is notoriously difficult to keep track of
the coherence conditions that this data must satisfy.
In contrast,
it is simple to check whether a geometric theory is well-defined,
and while it can in general contain arbitrarily big sets
(of relation and function symbols, say) as well,
the examples showing up in practice
are often more or less finitary.

It should be mentioned here that
syntactic presentations of toposes do always exist,
and there is a clear procedure
for constructing a classified geometric theory
out of a given site presentation of a topos.
But a presentation constructed in this way
will of course generally not tell us anything more about the topos
then the site itself does.
Whenever we speak of searching for syntactic presentations,
we intend to find a \emph{concise} presentation,
or one that is interesting for some other reason.

The first of our two main goals,
which will occupy us in Sections
\ref{section-extensions}
and
\ref{section-gluing},
will be to give
a construction on the level of geometric theories
for an operation which is very natural
when viewing toposes as generalized topological spaces,
namely the operation of gluing toposes
along open subtoposes.
More precisely,
our setup will be that $\E$ is a topos
covered by open subtoposes $\E_i = \E_{o(U_i)}$,
and syntactic presentations for the $\E_i$ are given.
Then we ask
how to construct a syntactic presentation for $\E$,
and what additional data might be needed for this.
The appropriate gluing data will consist, unsurprisingly,
of syntactic presentations of the intersections of the $\E_i$,
but not given independently of those for the $\E_i$,
but rather compatible with them, or, really, \emph{extending} them.
Here, the notion of extensions of geometric theories
will be crucial,
which will therefore be investigated first.
The formula we give for the theory classified by $\E$
(see Theorem \ref{theorem-main-gluing})
will then be quite elegant,
it simply adds up all the given theory extensions,
after transforming them into theory extensions
for \enquote{partial models} over the respective open subtoposes.
This gluing technique
is then applied to deduce a syntactic presentation
for the big Zariski topos of a non-affine scheme
(see Theorem \ref{theorem-gluing-Zar})
from the well-known result in the affine case.

Our second objective,
in Sections
\ref{section-presheaf-type}
and
\ref{section-crystalline},
is to give syntactic presentations
for another family of toposes from algebraic geometry,
namely the crystalline toposes of schemes.
These toposes were introduced around 1970
to study crystalline cohomology,
a tool for extracting geometric information from schemes,
similar to de Rham cohomology,
but specifically adapted to schemes
over ground fields of positive characteristic.
While more and more classified theories
for other toposes from algebraic geometry
were found over the years,
a syntactic presentation of the crystalline topos
was up to now missing.
The construction of a crystalline topos depends in fact
not on a single scheme,
as for the Zariski topos,
but on two schemes with some additional structure.
This is reflected in the more involved classified theory we give
(see Theorem \ref{theorem-affine-crystalline-classifies}),
but it is still very much related to the theory of local rings,
and the universal model living in the crystalline topos
consists precisely of its structure sheaf
and some additional data associated to it.
The case of affine base schemes is treated first,
and relies heavily on techniques
for recognizing theories of presheaf type
which we develop for this purpose.
It is then simply another application of the gluing theorem
to generalize to the non-affine case
(see Theorem \ref{theorem-gluing-Cris}),
although some extra care is needed
in constructing an open cover
and a system of syntactic presentations for it.


\ifdefined\assemblingchapters \else
\bibliography{bibliography.bib}
\end{document}
\fi
